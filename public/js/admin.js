// display all editors
displayEditor(document.getElementsByClassName("editor"));

function displayEditor(inputs){
    for(var i = inputs.length - 1; i >= 0; i--){
        new Pen(inputs[i]);
    }
}

// ????
$(".copy-content").on("click",function(){
    $(this).find("input").select();
    document.execCommand('copy');
});

// SUBMIT SAVE FORM
$(".admin form").on("submit",function(e){
    e.preventDefault();
    var data = {};
        data.input = [];

    var action = $(this).attr("action") || "form/save";

    getFormData($(this),data);
    data.page = $("body #_page").val();

    $.ajax({
        url: "/admin/"+action,
        type: "POST",
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            location.reload();
        },
        error: function (data) {
            console.log(data.responseText);
        }
    })
});

function getFormData(form,data){
    // INPUT
    $(".admin form input").each(function(){
        var temp = {
            type : "input",
            name : $(this).attr("name"),
            value : $(this).val()
        };
        data.input.push(temp);
    });

    $(".admin form textarea").each(function(){
        var temp = {
            type : "textarea",
            name : $(this).attr("name"),
            value : $(this).val()
        };
        data.input.push(temp);
    });

    // EDITORS
    $(".admin form .editor").each(function(){
        var temp = {
            type : "text",
            name : $(this).attr("name"),
            value : $(this).html()
        };
        data.input.push(temp);
    })
}
// SHOW POPUP AND CONTENT
$("body").on("click",".display-popup",function(e) {
    e.preventDefault();
    var pageToLoad = $(this).attr("href");
    $("body .popup").toggleClass("active");
    $("body .popup .loader").toggleClass("active");
    $( "body .popup .content").load(pageToLoad,function( response, status, xhr ) {
        if ( status === "error" ) {
            var msg = "Sorry but there was an error: ";
            console.log( msg + xhr.status + " " + xhr.statusText );
        }
    });
});
// EXIT POPUP
$("body").on("click",".popup .exit",function(e){
    $("body .popup").toggleClass("active");
});

// LIBRARY
$("body").on("click",".file-picker",function(e){

    // GETTING THE SOURCE OF THE SELECTED ELEMENT/ IMAGE
    var src = $(this).find("img").attr("src").replace("/s/150x150/",""),

    // GETTING THE TAG NAME OF THE INPUT
    tagName = $(this).parents(".library").find(".tag-selector").val();

    $("body input[name='"+tagName+"']").val(src);
    $("body ."+tagName+"-img").attr("src","/s/300/"+src).removeClass("hide");

    $("body .popup").toggleClass("active");
});

// TRIGGER WHEN USER ADDS AN IMAGE IN THE LIBRARY
$("body").on("change","#new-file-library",function(){
    var parents = $(this).parents(".library");
    parents.find(".loader").toggleClass("active");

    var params = {
            url: "/admin/library/create", // url: "/admin/upload-image",
            resize: true,
            data: $(this).get(0).files[0]
        },
        formData = new FormData;


    formData.append("new-image", params.data);

    $.ajax({
        url: params.url,
        type: "POST",
        data: formData,
        contentType: !1,
        cache: !1,
        processData: !1,
        mimeType: "multipart/form-data",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(imageSource) {
            parents.find(".loader").toggleClass("active");
            var el = '<li class="el"><a class="file-picker" href="#"><img src="/s/150x150/'+imageSource+'" alt=""></a></li>';
            parents.find(".img-list ul").prepend(el);
        },
        fail: function(a) {
            var r = {};
            r.status = 0, r.error = a, t(r)
        },
        error: function(data){
            console.log(data.responseText);
        }
    })
});
