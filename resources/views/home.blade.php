@extends("core.layout")

@section('content')
<main class="home">
    <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center center; ">
        <div class="overlay">
            <h2>{{$content["subtitle"] ?? ""}}</h2>
            <h2>{{$content["main-title"] ?? ""}}</h2>
       </div>
    </header>
    <section class="row row-center text-center">
        <q class="quote">{{$content["quote"] ?? ""}}<span class="author">{{$content["author"] ?? ""}}</span></q>
    </section>
    <section class="list-link row row-center">
        <ul class="row col-10 row-center">
            <li class="col-4 col-md-12">
                <a href="/coaching-mental">
                    <img style="height:200px;" src="/s/300/{{$content["image-1"] ?? ""}}" alt="">
                    <h2>{{$content["title-1"] ?? ""}}</h2>
                    <p>{{$content["textarea-1"] ?? ""}}</p>
                    <div class="link">
                        <span>En savoir plus</span>
                    </div>
                </a>
            </li>
            <li class="col-4 col-md-12">
                <a href="/image-de-soi">
                    <img style="height:200px;" src="/s/300/{{$content["image-3"] ?? ""}}" alt="">
                    <h2>{{$content["title-3"] ?? ""}}</h2>
                    <p>{{$content["textarea-3"] ?? ""}}</p>
                    <div class="link">
                        <span>En savoir plus</span>
                    </div>
                </a>
            </li>
            <li class="col-4 col-md-12">
                <a href="/enfants-ados">
                    <img style="height:200px;" src="/s/300/{{$content["image-2"] ?? ""}}" alt="">
                    <h2>{{$content["title-2"] ?? ""}}</h2>
                    <p>{{$content["textarea-2"] ?? ""}}</p>
                    <div class="link">
                        <span>En savoir plus</span>
                    </div>
                </a>
            </li>
        </ul>
    </section>
</main>
@endsection


