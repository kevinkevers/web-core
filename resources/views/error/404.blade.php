@extends("core.layout")

@section('content')
    <main class="e404">
        <p>The page you're looking for can't be found :(</p>
        <h1>404</h1>
        <a href="/@lang("routes.home")" class="cta">Go back home</a>
    </main>
@endsection