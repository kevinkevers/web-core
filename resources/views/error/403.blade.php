@extends("core.layout")

@section('content')
    <main class="e404">
        <p>{{ $exception->getMessage() }}</p>
        <h1>403</h1>
    </main>
@endsection