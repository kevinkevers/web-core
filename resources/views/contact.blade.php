@extends("core.layout")

@section('content')
<main class="contact">
    <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center;">
        <div class="overlay">
            <h1 class="title">{!! $content["page-title"] ?? "" !!}</h1>
        </div>
    </header>
    <section class="contact-form clearfix row">
        <div class="col-12">
            <h1>{!! $content["title"] ?? "" !!}</h1>
            {!! $content["text"] ?? "" !!}
        </div>
            <address class="left-side col-6 col-md-12">
                <a class="map" target="_blank" href="https://www.google.com/maps/place/Gentile+ch%C3%A2ssis/@50.6837145,5.5875936,13.88z/data=!4m5!3m4!1s0x0:0xe1f694896b36eaa4!8m2!3d50.6906128!4d5.5816233">
                    <img src="/img/map.png" alt="">
                </a>
                <ul class="infos">
                    <li class="location"><a href="">{!! $content["address"] ?? "" !!}</a></li>
                    <li class="phone"><a href="">{!! $content["phone"] ?? "" !!}</a></li>
                    <li class="mail"><a href="mailto:{!! $content["main-title"] ?? "" !!}">{!! $content["email"] ?? "" !!}</a></li>
                </ul>
            </address>
        <div class="right-side col-6 col-md-12">
            <form action="/contact/send-mail">
                <div class="input">
                    <input type="text" placeholder="Nom complet">
                </div>
                <div class="input">
                    <input class="half" type="text" placeholder="Téléphone"><input class="half" type="text" placeholder="Email">
                </div>
                <div class="input">
                    <textarea name="" placeholder="Votre message"></textarea>
                </div>
                <button class="submit cta">{!! $content["cta"] ?? "" !!}</button>
            </form>
        </div>
    </section>
</main>
@endsection


