@extends("core.layout")

@section('content')
<main class="enfants-ados">
   <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center;">
       <div class="overlay">
           <h1 class="title">{!! $content["main-title"] ?? "" !!}</h1>
       </div>
   </header>
    <section class="main-content col-12 container row row-center">
        <div class="col-8 col-md-12">
            <h1>{!! $content["title-1"] ?? "" !!}</h1>
            {!! $content["editor-1"] ?? "" !!}
        </div>
        <div class="col-6 col-md-12">
            <img src="/s/300/{!! $content["image-1"] ?? "" !!}" alt="">
        </div>
        <div class="col-6 col-md-12">
            <img src="/s/300/{!! $content["image-2"] ?? "" !!}" alt="">
        </div>
        <img src="{!! $content["image-1"] ?? "" !!}" alt="">
        <div class="row col-8 col-md-12 ow-spaced">
        <div class="col-5 col-md-12">
            <h2>Enfants</h2>
            <ul>
                <li>Avoir confiance en soi</li>
                <li>Gérer ses peurs</li>
                <li>Apprendre à gérer ses émotions</li>
                <li>Vaincre sa timidité</li>
                <li>Apprendre à gérer ses relations de groupe avec les copains</li>
                <li>Gérer ses peurs scolaires</li>
            </ul>
        </div>
        <div class="col-5 col-md-12">
            <h2>Ados</h2>
            <ul>
                <li>Développer la confiance en soi</li>
                <li>Apprendre à gérer ses émotions</li>
                <li>Vaincre sa timidité</li>
                <li>Apprendre à gérer les relations conflictuelles</li>
                <li>Se donner la capacité de se définir</li>
                <li>Apprendre à gérer ses relations de groupe </li>
                <li>Bénéficier de conseils en image adapté à l’adolescence</li>
                <li>Préparer un évènement importan</li>
                <li>Apprendre à gérer son stress en préparation d’un examen, d’un passage de permis de conduire,…</li>
                <li>Apprendre à se définir</li>
            </ul>
        </div>
        </div>
    </section>
</main>
@endsection


