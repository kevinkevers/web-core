@extends("core.layout")

@section('content')
<main class="news">
    <header style="background: url(/s/1000/{{$content["cover"] ?? ""}}); background-size: cover; background-position: center;">
        <div class="overlay">
            <h1 class="title">{{$content["title"] ?? ""}}</h1>
        </div>
    </header>
    <section class="all-news">
        <ul>
            <?php for($i=0;$i< count($articles);$i++){ ?>
                <li>
                    <a>
                        <div class="image">
                            <img src="/s/300/{{$articles[$i]->COVER_IMAGE}}" alt="bg">
                        </div>
                        <div class="content">
                            <span><?php echo (new DateTime($articles[$i]->PUBLISH_DATE))->format('d M Y') ?></span>
                            <h1>{{$articles[$i]->TITLE}}</h1>
                            {!! $articles[$i]->CONTENT !!}
                        </div>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </section>
</main>
@endsection


