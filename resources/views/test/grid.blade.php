PREAMBULE

Les parties souhaitent entamer des négociations mutuelles (ci - après les « Discussions ») susceptibles de conduire à un éventuel accord de collaboration (ci - après le « Projet ») sur la base d’une offre de l’ Utilisateur (ci - après « l’Offre »), qui sera annexée à la présente. Le « projet » consiste en

 Dans le cadre des Discussions et notamment pour évaluer l’opportunité du Projet et de l’Offre ou permettre à l’Utilisateur d’adapter celle - ci , la Société sera amenée à communiquer à l’Utilisateur des informations de toute nature, qui sont confidentielles et représentent des secrets d’affaires appartenant à la Société.

Les parties conviennent que ces informations, quelle que soit l’issue des Discussions,doivent demeurer strictement confidentielles conformément au présent accord