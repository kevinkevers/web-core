@extends("core.layoutAdmin")

@section('header') Dashboard @endsection


@section('content')
    <div class="col-12 row text-center dashboard no-gutter">
        <div class="card row col-8 page-list">
            <div class="col-2">
                <a href="/admin/editor/home">{!! svg("img/admin/docs.svg") !!} <span>Accueil</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/coaching-mental">{!! svg("img/admin/docs.svg") !!} <span>Le coaching mental</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/adulte-image-de-soi">{!! svg("img/admin/docs.svg") !!} <span>Adultes, image de soi</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/enfants-ados">{!! svg("img/admin/docs.svg") !!} <span>Enfants & Ados</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/a-propos">{!! svg("img/admin/docs.svg") !!} <span>A propos</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/contact">{!! svg("img/admin/docs.svg") !!} <span>Contact</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/modalites-pratiques">{!! svg("img/admin/docs.svg") !!} <span>Modalités pratiques</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/cadre-ethique">{!! svg("img/admin/docs.svg") !!} <span>Cadre Ethique</span></a>
            </div>
            <div class="col-2">
                <a href="/admin/editor/news">{!! svg("img/admin/docs.svg") !!} <span>News</span></a>
            </div>
        </div>

    </div>
@endsection