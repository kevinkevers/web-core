@extends("core.layoutAdmin")

@section('header')
    <h1>Edition de l'accueil</h1>
@endsection


@section('content')
    <div class="row">
        <div class="col col-2">
            <a class="cta" href="/admin/newsletter/list">View the list</a>
        </div>
        <div class="line-breaker"></div>
        <div class="form col-12">
            <div class="col-4"><input type="text"></div>
            <div class="col-2 select">
                <select name="" id="">
                    <option value="Hello">Hello</option>
                    <option value="Hello">Hello</option>
                    <option value="Hello">Hello</option>
                </select>
            </div>
            <div class="col-2">
                <button class="cta">Add</button>
            </div>
        </div>
    </div>

@endsection