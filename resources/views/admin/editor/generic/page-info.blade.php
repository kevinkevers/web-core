@extends("core.layoutAdmin")

@section('header')
    Edition de l'accueil
@endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a href="/admin/editor/home">Contenu</a></li>
            <li class="col gutter-right"><a class="active" href="/admin/editor/home/page-infos">Informations de la page</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <input id="_page" type="hidden" value="home">
    <form class="row col-8 col-lg-12 card">
        <div class="col-12">
            <h2>Titre de la page</h2>
            @include("admin.core.input", [
                "name" => "meta-page-title",
            ])
        </div>
        <div class="col-12">
            <h2>Description (max. 230 caractères)</h2>
            @include("admin.core.textarea", [
                "name" => "meta-page-description",
            ])
        </div>

        <div class="col-12">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "meta-page-image",
            ])
        </div>
        <div class="col-12">
            <button class="cta right">Enregistrer</button>
        </div>
    </form>

@endsection