@extends("core.layoutAdmin")

@section('header') Edition de news @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a class="active" href="/admin/editor/{{$subpage}}">Contenu</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/page-infos">Informations de la page</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/m/blog">Modifier les news</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <form class="row col-8 col-lg-12 card">
        <div class="col-12">
            <h2>Image de couverture</h2>
            @include("admin.core.image", [
                "name" => "cover"
            ])
        </div>
        <div class="col-12">
            <h2>Titre de la page</h2>
            @include("admin.core.input", [
                "name" => "title",
            ])
        </div>
        <div class="col-12">
            <button class="cta right">Enregistrer</button>
        </div>
    </form>
    
@endsection