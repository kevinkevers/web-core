@extends("core.layoutAdmin")

@section('header') Edition de contact @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a class="active" href="/admin/editor/{{$subpage}}">Contenu</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/page-infos">Informations de la page</a></li>
        </ul>
    </div>
@endsection

@section('content')

    <input id="_page" type="hidden" value="home">
    <form class="row col-8 col-lg-12 card">
        <div class="col-12">
            <h2>Image de couverture</h2>
            @include("admin.core.image", [
                "name" => "image-cover",
            ])
        </div>
        <div class="col-6">
            <h2>Titre de la page</h2>
            @include("admin.core.input", [
                "name" => "page-title",
                "type" => "text",
                "placeholder" => "Titre de la page",
            ])
        </div>
        <div class="col-6">
            <h2>Titre du contenu</h2>
            @include("admin.core.input", [
                "name" => "title",
                "type" => "text"
            ])
        </div>
        <div class="col-12">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "text",
                "type" => "text"
            ])
        </div>
        <div class="col-6">
            <h2>Adresse</h2>
            @include("admin.core.input", [
                "name" => "address",
                "type" => "text"
            ])
        </div>
        <div class="col-6">
            <h2>Téléphone</h2>
            @include("admin.core.input", [
                "name" => "phone",
                "type" => "text"
            ])
        </div>
        <div class="col-6">
            <h2>Email</h2>
            @include("admin.core.input", [
                "name" => "email",
                "type" => "text"
            ])
        </div>

        <div class="col-6">
            <h2>Intitulé du bouton</h2>
            @include("admin.core.input", [
                "name" => "cta",
                "type" => "text"
            ])
        </div>

        <div class="col-12 right-content">
            <button class="cta">Enregistrer</button>
        </div>
    </form>
    
@endsection