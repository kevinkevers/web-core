@extends("core.layoutAdmin")

@section('header') Edition de coaching mental @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a class="active" href="/admin/editor/{{$subpage}}">Contenu</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/page-infos">Informations de la page</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <form class="row col-8 col-lg-12 card">
        <div class="col-12">
            <h2>Image de couverture</h2>
            @include("admin.core.image", [
                "name" => "image-cover"
            ])
        </div>
        <div class="col-12">
            <h2>Titre de la page</h2>
            @include("admin.core.input", [
                "name" => "main-title",
            ])
        </div>
        <div class="col-6 row no-gutter">
            <div class="col-12">
                <h2>Titre</h2>
                @include("admin.core.input", [
                    "name" => "title",
                ])
            </div>
            <div class="col-12">
                <h2>Citation</h2>
                @include("admin.core.textarea", [
                    "name" => "quote",
                ])
            </div>
        </div>
        <div class="col-6">
            <h2>Image de profil</h2>
            @include("admin.core.image", [
                "name" => "image",
            ])
        </div>
        <div class="col-6">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "content",
            ])
        </div>
        <div class="line-breaker"></div>
        <div class="col-4">
            <h2>Bouton</h2>
            @include("admin.core.input", [
                "name" => "button",
            ])
        </div>
        <div class="col-12">
            <button class="cta right">Enregistrer</button>
        </div>
    </form>
    
@endsection