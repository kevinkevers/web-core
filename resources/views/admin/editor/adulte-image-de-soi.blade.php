@extends("core.layoutAdmin")

@section('header') Edition de Adultes, l'image de soi @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a class="active" href="/admin/editor/{{$subpage}}">Contenu</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/page-infos">Informations de la page</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <form class="row col-8 col-lg-12 card">
        <div class="col-12">
            <h2>Image de couverture</h2>
            @include("admin.core.image", [
                "name" => "image-cover"
            ])
        </div>
        <div class="col-12">
            <h2>Titre principal</h2>
            @include("admin.core.input", [
                "name" => "main-title",
            ])
        </div>

        <div class="col-12">
            <h2>Titre contenu</h2>
            @include("admin.core.input", [
                "name" => "title-1",
            ])
        </div>
        <div class="col-12">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "editor-1",
            ])
        </div>
        <div class="col-12">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "image-1",
            ])
        </div>
        <div class="col-12">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "editor-2",
            ])
        </div>
        <div class="col-12">
            <h2>Titre 2</h2>
            @include("admin.core.input", [
                "name" => "title-2",
            ])
        </div>
        <div class="col-12">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "image-2",
            ])
        </div>
        <div class="col-12">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "editor-3",
            ])
        </div>
        <div class="col-8">
            <h2>Citation</h2>
            @include("admin.core.input", [
                "name" => "quote",
            ])
        </div>
        <div class="col-4">
            <h2>Auteur</h2>
            @include("admin.core.input", [
                "name" => "author",
            ])
        </div>
        <div class="col-12">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "editor-4",
            ])
        </div>
        <div class="col-6">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "image-3",
            ])
        </div>
        <div class="col-6">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "image-4",
            ])
        </div>
        <div class="col-12">
            <button class="cta right">Enregistrer</button>
        </div>
    </form>

@endsection