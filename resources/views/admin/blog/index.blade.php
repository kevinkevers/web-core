@extends("core.layoutAdmin")

@section('header') Edition de news @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}">Contenu</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/page-infos">Informations de la page</a></li>
            <li class="col gutter-right"><a class="active" href="/admin/editor/{{$subpage}}/m/blog">Modifier les news</a></li>
        </ul>
    </div>
@endsection
@section('content')
        <div class="row col-12">
            <div class="col-12 no-gutter-left">
                <a href="blog/view/new" class="cta">Créer une nouvelle news</a>
            </div>

                <section class="list col-8">
                    <ul class="labels row col-12">
                        <li class="col-8 no-gutter"><a href="">Titre</a></li>
                        <li class="col-4 no-gutter"><a href="">Date de publication</a></li>
                    </ul>
                    <ul class="articles row col-12 no-gutter-side">
                        <?php for($i=0;$i< count($content);$i++){ ?>
                        <li class="col-12 no-gutter">
                            <a href="blog/view/{{$content[$i]->ID}}" class="col-12 row">
                                <div class="col-8 col-md-12 no-gutter">{{$content[$i]->TITLE}}</div>
                                <div class="col-4 no-gutter">
                                    <?php
                                    echo (new DateTime($content[$i]->PUBLISH_DATE))->format('d M Y');
                                    ?>

                                </div>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </section>
        </div>

@endsection