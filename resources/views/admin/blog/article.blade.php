@extends("core.layoutAdmin")

@section('header') Edition de coaching mental @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}">Contenu</a></li>
            <li class="col gutter-right"><a href="/admin/editor/{{$subpage}}/page-infos">Informations de la page</a></li>
            <li class="col gutter-right"><a class="active" href="/admin/editor/{{$subpage}}/m/blog">Modifier les news</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="col-12">
        <a class="back" href="{{ url()->previous() }}">Retourner à la liste des news</a>
    </div>
    <form action="m/blog/saveArticle" class="row col-8 col-lg-12 card">
        <input type="hidden" name="new_or_update" value="{!!$content["ID"] ?? "" !!}">
        <div class="col-12">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "COVER_IMAGE"
            ])
        </div>
        <div class="col-12">
            <h2>Titre de l'article</h2>
            @include("admin.core.input", [
                "name" => "TITLE",
            ])
        </div>
        <div class="col-12">
            <h2>Texte</h2>
            @include("admin.core.editor", [
                "name" => "CONTENT",
            ])
        </div>
        <div class="col-12">
            <button class="cta right">Enregistrer</button>
        </div>
    </form>
    
@endsection