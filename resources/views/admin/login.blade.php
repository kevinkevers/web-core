@php
    $root = 'https://'.$_SERVER['HTTP_HOST'];

@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name=”viewport” content=”width=device-width, initial-scale=1″>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex">

        {{-- STYLESHEET --}}
        <link rel="stylesheet" type="text/css" href="/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="/css/admin.css"/>

        <title>Administrate your website</title>

    </head>
    <body>
    <main class="login admin">
        <div class="wrapperLogin">
            <h1>Connexion à l'administration</h1>
            <div class="container">
                <img src="/img/admin/tools.svg" alt="Tools">
                <form action="/admin/login/check" method="post">
                    <input id="username-admin" type="text" name="username" placeholder="Username" value="admin"/>
                    <input id="password-admin" type="password" name="password" placeholder="Password" value=""/>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <button class="cta">Se connecter</button>
                </form>
                @if(Session::has('error'))
                    <div class="error">{{Session::get("error")}} </div>
                @endif
            </div>
        </div>
    </main>
    <script src="{{ elixir('js/main.js') }}"></script>
    </body>
</html>