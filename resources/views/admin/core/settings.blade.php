@extends("core.layoutAdmin")

@section('header') Paramètres du site @endsection

@section('header-menu')
    <div class="col-12 gutter-side">
        <ul class="header-menu row col-12 gutter-top">
            <li class="col gutter-right"><a href="/admin/settings">Général</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="popup">
        <div class="overlay"></div>
        <div class="content row">
            <div class="loader position-center"></div>
        </div>
    </div>
    <input id="_page" type="hidden" value="home">
    <form class="row col-8 col-lg-12 card">
        <div class="col-12">
            <h2>Facebook</h2>
            @include("admin.core.input", [
                "name" => "facebook",
            ])
        </div>
        <div class="col-12">
            <h2>Twitter</h2>
            @include("admin.core.input", [
                "name" => "twitter",
            ])
        </div>
        <div class="col-12">
            <h2>Linkedin</h2>
            @include("admin.core.input", [
                "name" => "linkedin",
            ])
        </div>
        <div class="col-12">
            <h2>Description (max. 230 caractères)</h2>
            @include("admin.core.textarea", [
                "name" => "meta-page-description",
            ])
        </div>

        <div class="col-12">
            <h2>Image</h2>
            @include("admin.core.image", [
                "name" => "meta-page-image",
            ])
        </div>
        <div class="col-12">
            <button class="cta right">Enregistrer</button>
        </div>
    </form>

@endsection