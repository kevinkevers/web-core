<div class="image-editor">
    <a href="/admin/library?tag={{$name}}" class="display-popup">
        <div class="image">
            @if($content[$name] ?? false)
                <input name="{{$name}}" type="hidden" value="{{$content[$name] ?? ""}}">
                <img class='current-image {{$name}}-img' src="/s/300/{{$content[$name] ?? ""}}">
            @else
            <?php include("img/admin/picture.svg")?>
                <input name="{{$name}}" type="hidden" value="">
                <img class="hide current-image {{$name}}-img" src="/s/300/{{$content[$name] ?? ""}}" alt="">
            @endif
        </div>
    </a>
</div>

<!--
             <div class="loader position-center"></div>
            <input id="new-image" name="" class="new-main-image hide" type="file"/>
 -->