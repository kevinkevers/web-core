<div class="library">
    <div class="loader position-center"></div>
    <header class="col-12">
        <input class="tag-selector" type="hidden" value="{{$tag}}">
        <button class="exit"><?php include("img/admin/cross.svg")?></button>
        <h1>Library</h1>
        <div class="action">
            <label class="upload-file-label" for="new-file-library">+ Uploader une photo</label>
            <input id="new-file-library" name="new-file-library" class="hide" type="file">
        </div>
    </header>
    <div class="img-list">
            <?php if(isset($images)){
                ?>
                <ul class="col-12">
                    <?php
                $nbr = count($images);
                for($i=0;$i< $nbr; $i++){

                ?>
            <li class="el">
                <a class="file-picker" href="#"><img src="/s/150x150/{{$images[$i]->PATH}}" alt=""></a>
            </li>

            <?php }
            ?>
                </ul>
                <?php
            } else{
                echo "Pas d'images dans la bibliothèque";
            }


            ?>

    </div>
</div>