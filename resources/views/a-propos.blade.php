@extends("core.layout")

@section('content')
<main class="a-propos">
        <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center center; ">
        <div class="overlay">
            <h1 class="title">{!! $content["main-title"] ?? "" !!}</h1>
        </div>
    </header>
    <section class="about">
        <div class="clearfix">
            <div class="left-content">
                <h1>{!! $content["title"] ?? "" !!}</h1>
                <q>{!! $content["quote"] ?? "" !!}</q>
                {!! $content["content"] ?? "" !!}
                <div class="link">
                    <a href="/coaching-mental">{!! $content["button"] ?? "" !!}</a>
                </div>
            </div>
            <div class="right-content">
                <img src="/s/300/{!! $content["image"] ?? "" !!}" alt="Image de profil">
            </div>
        </div>
    </section>
</main>
@endsection


