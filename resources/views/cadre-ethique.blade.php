@extends("core.layout")

@section('content')
<main class="coaching-mental">
    <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center;">
        <div class="overlay">
            <h1 class="title">{!! $content["main-title"] ?? "" !!}</h1>
        </div>
    </header>
    <section class="main-content row col-12 container row-center">
        <div class="col-8 col-md-12 container">
            <h1>{!! $content["title-1"] ?? "" !!}</h1>
            {!! $content["editor-1"] ?? "" !!}
            <img src="/s/300/{!! $content["image-1"] ?? "" !!}" alt="">
            {!! $content["editor-2"] ?? "" !!}
        </div>
<!--
        <div class="container col-8">
                <ul class="flex">
                    <li>
                        <div class="pix">
                            <img src="/img/censored.svg" alt="">
                        </div>
                        <p>Le coach mental est tenu par le respect du caractère strictement confidentiel des informations transmises par le client</p>
                    </li>
                    <li>
                        <div class="pix">
                            <img src="/img/skills.svg" alt="">
                        </div>
                        <p>Le coach mental s’engage à mettre toutes ses compétences développées au fil du temps à disposition du client (formations, supervisions, etc ...) et à poursuivre son développement personnel</p>
                    </li>
                    <li>
                        <div class="pix">
                            <img src="/img/objective.svg" alt="">
                        </div>
                        <p>Le client est responsable de l’atteinte de ses objectifs, le résultat du coaching dépend de sa volonté d’évolution mais aussi de sa capacité à mettre les moyens en place pour y parvenir</p>
                    </li>
                    <li>
                        <div class="pix">
                            <img src="/img/locked.svg" alt="">
                        </div>
                        <p>Le client et le coach mental sont conscients des règles déontologiques et de confidentialité auxquelles est soumis le coach mental</p></li>
                </ul>
            </div>
-->

    </section>
</main>
@endsection


