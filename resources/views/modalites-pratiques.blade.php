@extends("core.layout")

@section('content')
<main class="coaching-mental">
    <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center;">
        <div class="overlay">
            <h1 class="title">{!! $content["main-title"] ?? "" !!}</h1>
        </div>
    </header>
    <section class="main-content row col-12">
        <div class="text-center col-12">
            <q class="quote">{!! $content["quote"] ?? "" !!}<span class="author">{!! $content["author"] ?? "" !!}</span></q>
        </div>
        <div class="container col-4 col-md-12">
            <h1>{!! $content["title-1"] ?? "" !!}</h1>
            {!! $content["editor-1"] ?? "" !!}
            <img src="/s/300/{!! $content["image-1"] ?? "" !!}" alt="">
            <h1>{!! $content["title-2"] ?? "" !!}</h1>
            {!! $content["editor-2"] ?? "" !!}
        </div>
    </section>
</main>
@endsection


