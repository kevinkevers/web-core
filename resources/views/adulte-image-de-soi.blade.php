@extends("core.layout")

@section('content')
<main class="coaching-mental">
    <header style="background: url(/s/1000/{!! $content["image-cover"] ?? "" !!}); background-size: cover; background-position: center;">
        <div class="overlay">
            <h1 class="title">{!! $content["main-title"] ?? "" !!}</h1>
        </div>
    </header>
    <section class="main-content container row col-12 row-center">
        <div class="col-8 col-md-12">
            <h1>{!! $content["title-1"] ?? "" !!}</h1>
            {!! $content["editor-1"] ?? "" !!}
            <img src="/s/300/{!! $content["image-1"] ?? "" !!}" alt="">
            {!! $content["editor-2"] ?? "" !!}

            <h1>{!! $content["title-2"] ?? "" !!}</h1>
            <img src="/s/300/{!! $content["image-2"] ?? "" !!}" alt="">
            {!! $content["editor-3"] ?? "" !!}
            <div class="text-center col-12">
                <q class="quote">{!! $content["quote"] ?? "" !!}<span class="author">{!! $content["author"] ?? "" !!}</span></q>
            </div>
            {!! $content["editor-4"] ?? "" !!}
            <div class="col-6 col-md-12">
                <img src="/s/300/{!! $content["image-3"] ?? "" !!}" alt="">
            </div>
            <div class="col-6 col-md-12">
                <img src="/s/300/{!! $content["image-4"] ?? "" !!}" alt="">
            </div>
            </div>

    </section>
</main>
@endsection


