@php
    $root = 'http://'.$_SERVER['HTTP_HOST'];

    if(!isset($page)){
        $page = "";
    }
    if(!isset($subpage)){
        $subpage = "";
    }
function checkActive($var,$value){
    if ($var && $var === $value)
        echo "active";
}
@endphp

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Manager</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="/css/admin.css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/img/admin/favicon.png" />
</head>

<body class="admin">
<div class="manager row col-12 no-gutter">
@include("partials.notification");
    <div class="navigation">
        <header>
            <a href="/admin/dashboard"><?php include("img/admin/infinite.svg")?></a>
        </header>
        <ul class="main-nav">
            <li>
                <a href="/admin/editor/home" id="editor" class="<?php checkActive($page,"editor"); ?>"> <?php include("img/admin/editor.svg")?><span>Editor</span></a>
                <a href="/admin/settings" class="<?php checkActive($page,"settings"); ?>"><?php include("img/admin/settings.svg") ?> <span>Paramètres</span></a>
                <a href="/admin/library" class="<?php checkActive($page,"library"); ?>"><?php include("img/admin/library.svg") ?> <span>Bibliothèque</span></a>
                <a class="logout" href="/admin/logout"><?php include("img/admin/logout.svg")?></a>
                    <div class="sub-menu-to-display <?php checkActive($page,"editor"); ?>">
                        <h1>Pages</h1>
                        <ul>
                            <li><a href="/admin/editor/home" class="<?php checkActive($subpage,"home"); ?>"> <?php include("img/admin/editor.svg")?>Accueil</a></li>
                            <li><a href="/admin/editor/coaching-mental" class="<?php checkActive($subpage,"coaching-mental"); ?>"><?php include("img/admin/editor.svg")?>Le coaching mental</a></li>
                            <li><a href="/admin/editor/adulte-image-de-soi" class="<?php checkActive($subpage,"adulte-image-de-soi"); ?>"><?php include("img/admin/editor.svg")?>Adultes, image de soi</a></li>
                            <li><a href="/admin/editor/enfants-ados" class="<?php checkActive($subpage,"enfants-ados"); ?>"><?php include("img/admin/editor.svg")?>Enfants, ados</a></li>
                            <li><a href="/admin/editor/a-propos" class="<?php checkActive($subpage,"a-propos"); ?>"><?php include("img/admin/editor.svg")?>A propos</a></li>
                            <li><a href="/admin/editor/contact" class="<?php checkActive($subpage,"contact"); ?>"><?php include("img/admin/editor.svg")?>Contact</a></li>
                            <li><a href="/admin/editor/modalites-pratiques" class="<?php checkActive($subpage,"modalites-pratiques"); ?>"><?php include("img/admin/editor.svg")?>Modalités Pratiques</a></li>
                            <li><a href="/admin/editor/cadre-ethique" class="<?php checkActive($subpage,"cadre-ethique"); ?>"><?php include("img/admin/editor.svg")?>Cadre Ethique</a></li>
                            <li><a href="/admin/editor/news" class="<?php checkActive($subpage,"news"); ?>"><?php include("img/admin/editor.svg")?>News</a></li>
                        </ul>
                    </div>
            </li>
        </ul>
    </div>
    <header class="admin-header row col gutter-side">
        <h1 class="v-center col-6 col-lg-12"> @section('header')@show </h1>
        @section('header-menu')@show
    </header>
    <main class="main-content row" id="main-content">
        <div class="popup">
            <div class="overlay"></div>
            <div class="content row">
                <div class="loader position-center"></div>
            </div>
        </div>
        <input id="_page" type="hidden" value="{{$subpage ?? "null"}}">
        @section('content')@show
    </main>
</div>
<script src="/js/lib.js"></script>
<script src="/js/main.js"></script>
<script src="/js/all.js"></script>
<script src="/js/admin.js"></script>
<script>
</script>
</body>
</html>