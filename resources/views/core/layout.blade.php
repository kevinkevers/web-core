@php
    $root = 'https://'.$_SERVER['HTTP_HOST'];

@endphp
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta property="description" content="@lang("description")"> {{-- MAX 160 char--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>My Custom Coaching &#124; Virginie Lezaack</title>
    {{-- SOCIAL MEDIAS --}}
    <meta property="og:url" content="@php echo url()->full(); @endphp"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="My Custom Coaching &#124; Virginie Lezaack"/>
    <meta property="og:description" content="My Custom Coaching  &#124; « Pour réaliser une chose vraiment extraordinaire, commencez par la rêver. Ensuite, réveillez-vous calmement et allez d'un trait jusqu'au bout de votre rêve sans jamais vous laisser décourager. »"/>
    <meta property="og:image" content="/img/og-image-1280x630-frbe.png"/>

    {{-- STYLESHEET --}}
    <link rel="stylesheet" type="text/css" href="/css/main.css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Slab:300,400,700" rel="stylesheet">

    <link rel="canonical" href="https://www.mycustomcoaching.be">

    {{-- FAVICON --}}
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#ff5a5a">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
</head>
<body class="website">
<header class="main-header">
    <div class="container clearfix">
        <a class="logo" href="/">
            <h1 class="title"><img src="/img/logo.png" alt="logo"><span class="hide">My Custom Coaching</span></h1>
        </a>
        <nav class="main-nav">
            <ul class="cat-list">
                <li><a href="/accueil">Acceuil</a></li>
                <li class="dd-menu">
                    <a href="/accueil">My Custom Coaching</a>
                    <ul>
                        <li>
                            <a href="/coaching-mental">Le coaching mental</a>
                            <a href="/image-de-soi">Adultes, l'image de soi</a>
                            <a href="/enfants-ados">Enfants & ados</a>
                        </li>
                    </ul>
                </li>
                <li><a href="/modalites-pratiques">Modalités pratiques</a></li>
                <li><a href="/cadre-ethique">Cadre éthique</a></li>
                <li><a href="/a-propos">A propos</a></li>
                <li><a href="/contact">Contact</a></li>
                <li><a href="/news">News</a></li>
            </ul>
        </nav>
    </div>
    <button class="burger-menu"><?php include("img/admin/menu.svg"); ?></button>
</header>

@section('content')@show
<footer class="main-footer col-12">
    <div class="flex">
        <div>
            <h1 class="title"><img src="/img/logo.png" alt="logo"><span class="hide">My Custom Coaching</span></h1>
        </div>
        <div>
            <ul class="social clearfix">
                <li>
                    <a href=""><?php include("img/social/facebook.svg"); ?><span class="hide">Facebook</span></a>
                </li>
                <li>
                    <a href=""><?php include("img/social/twitter.svg"); ?><span class="hide">Twitter</span></a>
                </li>
                <li>
                    <a href=""><?php include("img/social/linkedin.svg"); ?><span class="hide">Linkedin</span></a>
                </li>
            </ul>
        </div>
    </div>
    <hr>
    <div class="flex">
        <div>
            <small>&copy; <?php echo Date("Y"); ?> My Custom Coaching. Tous droits réservés</small>
        </div>
        <div>
            <span class="kevinkevers"><a href="http://www.kevinkevers.be" target="_blank"><?php include("img/kevinkevers.svg"); ?></a></span>
        </div>
    </div>
</footer>
<script src="{{ elixir('js/lib.js') }}"></script>
<script src="{{ elixir('js/main.js') }}"></script>
</body>
</html>