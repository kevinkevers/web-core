<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home'          =>    'home',
    'explore'       =>    'explore',
    'responses'     =>    'responses',
    'about'         =>    'about',
    'contact'       =>    'contact',
    'terms'         =>    'terms',
    'career'        =>    'career',
    'blog'          =>    'blog',
    'faq'           =>    'faq',
    'profile'       =>    'profile',
    'requests'      =>    'requests',
    'signup'        =>    'signup',
    'login'         =>    'login',
    'edit'          =>    'edit',
    'verification'  =>    'verification',
    'users'         =>    'users',
    'account'       =>    'account',


    // CATEGORIES

    'home-improvements' => 'home-improvements',
    'wellness'          => 'wellness',
    'pets'              => 'pets',
    'gardening'         => 'gardening',
    'event'             => 'event',
    'lessons'           => 'lessons',
    'legal'             => 'legal',
    'photography'       => 'photography',
    'text'              => 'text',
    'business'          => 'business',
    'technical-and-repair' => 'technical-and-repair',
    'craft'             => 'craft',

    // SUBCATEGORIES

];
