<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login'                  =>    'Email and/or password are incorrect',
    'email-required'          =>    'Email is required.',
    'email-valid'          =>    'Email is not valid.',
    'first-name-required'          =>    'First name is required.',
    'last-name-required'          =>    'Last name is required.',
    'password-required'          =>    'Password is required.',

];
