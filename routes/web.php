<?php

Route::get('/', 'CoreController@index')->name("home");
Route::get('/accueil', 'CoreController@index')->name("home");
Route::get('/a-propos', 'CoreController@index')->name("a-propos");
Route::get('/contact', 'CoreController@index')->name("contact");
Route::get('/coaching-mental', 'CoreController@index')->name("coaching-mental");

Route::get('/modalites-pratiques', 'CoreController@index')->name("modalites-pratiques");
Route::get('/cadre-ethique', 'CoreController@index')->name("cadre-ethique");
Route::get('/enfants-ados', 'CoreController@index')->name("enfants-ados");
Route::get('/image-de-soi', 'CoreController@index')->name("adulte-image-de-soi");

Route::get('/news', 'HomeController@indexNews')->name("news");

Route::group(['prefix' => 'test'], function () {
    Route::get('/grid', function () {
        return view("test.grid");
    });
    Route::get('/phpinfo', function () {
        return view("test.phpinfo");
    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', 'Admin\LoginController@index');
    Route::post('login/check', 'Admin\LoginController@login');
    Route::get('logout', 'Admin\LoginController@login');
    Route::group(['middleware' => 'authenticated'], function () {
        Route::get('/', function () {
            return view("admin.dashboard", ["page" => "dashboard"]);
        });
        Route::get('dashboard', function () {
            return view("admin.dashboard", ["page" => "dashboard"]);
        });

        Route::match(['get', 'post'],'m/{module}/{action?}','Admin\ManagerController@moduleLauncher');
        Route::match(['get', 'post'],'{page}/{subpage}/m/{module}/{action?}/{param?}','Admin\ManagerController@moduleLauncher');

        Route::get('editor/{page}/{tab}', 'Admin\ManagerController@indexEditor');
        Route::get('editor/{page}', 'Admin\ManagerController@indexEditor');

        Route::post('save-data', 'Admin\ManagerController@saveData');

        Route::get('library', 'Admin\LibraryController@index');
        Route::get('settings', function () {
            return view("admin.core.settings",["page" => "settings"]);
        });


        Route::post('/upload-image', 'Admin\LibraryController@uploadImage');
        Route::post('/library/create', 'Admin\LibraryController@create');
        Route::post('/form/save', 'Admin\ManagerController@saveData');
    });
});

// STORAGE
Route::get('/s/{url}', 'Admin\ManagerController@getFile')->where('url','.+');

Route::post('/testme', 'Admin\LibraryController@test');
Route::get('/testme', 'Admin\LibraryController@testGet');

// DISPLAY AJAX
// oute::get('/a/{url}', 'Admin\ManagerController@getFile')->where('url','.+');

