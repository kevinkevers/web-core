<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('ID');
            $table->tinyInteger('NOTE')->unsigned();
            $table->integer('FROM')->unsigned();
            $table->integer('TO')->unsigned();
            $table->integer('WORK')->unsigned();
            $table->string('COMMENT',1024);
           	$table->timestamp('DATE');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
