<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users_conversations', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
            $table->foreign('CONVERSATION')->references('ID')->on('conversations');
        });
        Schema::table('conversations', function (Blueprint $table) {
            $table->foreign('WORK')->references('ID')->on('works');
        });

        Schema::table('verifications', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
        });

        Schema::table('messages', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
            $table->foreign('CONVERSATION')->references('ID')->on('conversations');
        });
        Schema::table('users_works', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
            $table->foreign('WORK')->references('ID')->on('works');
        });
        Schema::table('works', function (Blueprint $table) {
            $table->foreign('WORKER')->references('ID')->on('users');
            $table->foreign('ASKER')->references('ID')->on('users');
            $table->foreign('ADDRESS')->references('ID')->on('addresses');
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->foreign('FROM')->references('ID')->on('users');
            $table->foreign('TO')->references('ID')->on('users');
            $table->foreign('WORK')->references('ID')->on('works');
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->foreign('OWNER')->references('ID')->on('users');
        });
        Schema::table('users_groups', function (Blueprint $table) {
            $table->foreign('GROUP')->references('ID')->on('groups');
            $table->foreign('USER')->references('ID')->on('users');
        });
        Schema::table('albums', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
        });
        Schema::table('pictures', function (Blueprint $table) {
            $table->foreign('ALBUM')->references('ID')->on('albums');
        });
        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('MAIN')->references('ID')->on('users');
            $table->foreign('CONTACT')->references('ID')->on('users');
        });
        Schema::table('users_languages', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
            $table->foreign('LANGUAGE')->references('ID')->on('languages');
        });
        Schema::table('warnings', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
        });
        Schema::table('users_badges', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
            $table->foreign('BADGE')->references('ID')->on('badges');
        });
        Schema::table('sessions', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
        });
        Schema::table('addresses', function (Blueprint $table) {
            $table->foreign('USER')->references('ID')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
