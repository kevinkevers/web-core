<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('USER')->unsigned();
            $table->string('NUMBER',8);
            $table->string('STREET',64);
            $table->integer('ZIP');
            $table->string('CITY',64);
            $table->string('COUNTRY',32);
            $table->Point('LOCATION');
            $table->spatialIndex('LOCATION');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
