<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('TOKEN', 128);
            $table->string('FIRST_NAME',64);
            $table->string('LAST_NAME',64);
            $table->date('BIRTHDAY')->nullable()->default(null);
            $table->string('SITE_LANG',3);
            $table->text('BIOGRAPHY')->nullable()->default(null);
            $table->boolean('GENDER')->nullable()->default(null);
            $table->string('PHONE_NUMBER',15)->nullable()->default(null);
            $table->boolean('PHONE_CHECKED')->default(0);
            $table->string('PROFILE_PICTURE',128)->nullable()->default(null);
            $table->string('COVER_PICTURE',128)->nullable()->default(null);
            $table->string('FACEBOOK',64)->nullable()->default(null);
            $table->string('LINKEDIN',64)->nullable()->default(null);
            //$table->string('ADDRESS_NUMBER',8)->nullable()->default(null);
            //$table->string('ADDRESS_STREET',64)->nullable()->default(null);
            //$table->integer('ADDRESS_ZIP')->nullable()->default(null);
            //$table->string('ADDRESS_CITY',64)->nullable()->default(null);
            //$table->string('ADDRESS_COUNTRY',32)->nullable()->default(null);
            $table->double('AVERAGE_REVIEW')->nullable()->default(null);
            $table->integer('RANK')->nullable()->default(null);
            $table->integer('RANK_PROGRESS')->nullable()->default(null);
            $table->string('EMAIL',64);
            $table->boolean('EMAIL_CHECKED')->default(0);
            $table->string('PASSWORD',256);
            $table->tinyInteger('ANIMALS')->default(0);
            $table->tinyInteger('CIGARETTE')->default(0);
            $table->double('CASH')->default(0);
            $table->boolean('RULES_SIGNED')->default(0);
            $table->timestamp('CREATION_DATE');
	        $table->timestamp('LAST_CONNECTION');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
