<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('WORKER')->unsigned();
            $table->integer('ASKER')->unsigned();
            $table->double('PRICE');
            $table->integer('STATUT');
            //$table->string('ADDRESS_NUMBER',8);
            //$table->string('ADDRESS_STREET',64);
            //$table->integer('ADDRESS_ZIP');
            //$table->string('ADDRESS_CITY',64);
            //$table->string('ADDRESS_COUNTRY',32);
            $table->integer('ADDRESS')->unsigned();
            $table->integer('SECTION')->unsigned();
           	$table->timestamp('DATE');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
