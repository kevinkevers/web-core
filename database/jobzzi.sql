-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 09, 2018 at 04:07 PM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `STREJOB`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `NAME` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `badges`
--

CREATE TABLE `badges` (
  `ID` int(10) UNSIGNED NOT NULL,
  `NAME` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `ID` int(10) UNSIGNED NOT NULL,
  `MAIN` int(10) UNSIGNED NOT NULL,
  `CONTACT` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `ID` int(10) UNSIGNED NOT NULL,
  `DATE_LAST_MESSAGE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `WORK` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `ID` int(10) UNSIGNED NOT NULL,
  `OWNER` int(10) UNSIGNED NOT NULL,
  `NAME` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PICTURE` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `ID` int(10) UNSIGNED NOT NULL,
  `NAME` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `ID` int(10) UNSIGNED NOT NULL,
  `CONTENT` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `CONVERSATION` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_01_24_132049_create_users_table', 1),
(2, '2018_01_24_150540_create_users_conversations_table', 1),
(3, '2018_01_24_150620_create_conversations_table', 1),
(4, '2018_01_24_150659_create_messages_table', 1),
(5, '2018_01_24_150726_create_users_works_table', 1),
(6, '2018_01_24_150743_create_works_table', 1),
(7, '2018_01_24_150922_create_reviews_table', 1),
(8, '2018_01_24_151216_create_groups_table', 1),
(9, '2018_01_24_151243_create_users_groups_table', 1),
(10, '2018_01_24_151301_create_albums_table', 1),
(11, '2018_01_24_151318_create_pictures_table', 1),
(12, '2018_01_24_151335_create_contacts_table', 1),
(13, '2018_01_24_151401_create_users_languages_table', 1),
(14, '2018_01_24_151508_create_languages_table', 1),
(15, '2018_01_24_151525_create_warnings_table', 1),
(16, '2018_01_24_151601_create_notifications_table', 1),
(17, '2018_01_24_151648_create_users_badges_table', 1),
(18, '2018_01_24_151707_create_badges_table', 1),
(19, '2018_01_24_151707_create_sessions_table', 1),
(20, '2018_01_26_124340_foreign_keys', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `TITLE` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CONTENT` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATE` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `ID` int(10) UNSIGNED NOT NULL,
  `ALBUM` int(10) UNSIGNED NOT NULL,
  `NAME` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PICTURE` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `ID` int(10) UNSIGNED NOT NULL,
  `NOTE` tinyint(3) UNSIGNED NOT NULL,
  `FROM` int(10) UNSIGNED NOT NULL,
  `TO` int(10) UNSIGNED NOT NULL,
  `WORK` int(10) UNSIGNED NOT NULL,
  `COMMENT` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `TOKEN` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IP` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`ID`, `USER`, `TOKEN`, `IP`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$JFYQ0msK6XjqeR94QCBi5uke/Je7Ttg8PW/s4YqEqnwRn0Pfau/i.', '::1', NULL, NULL),
(2, 1, '$2y$10$mL6gfv8jTyGc3S/T1b4UtO.exlJsGtNtnsQSglQoeGyLECFNSIGCW', '::1', NULL, NULL),
(3, 1, '$2y$10$KE.kqwy01C75yjs40CjeBOVPCcF2leaVTRVoJ0zkCVR4aEmnAoJYe', '::1', NULL, NULL),
(4, 1, '$2y$10$wJIWwjaFBXSEmjtcRyEXgurhc1zC1Zzg72V1YWfilpoCILXnbd0YS', '::1', NULL, NULL),
(5, 1, '$2y$10$8G42zYIpKuS5G8SQqrzgxeRL3I/pRm6vRb.WgL0jme5AGT7zD7tIq', '::1', NULL, NULL),
(6, 1, '$2y$10$MCicNIe8Bcos4XV5EYb2xOmIEiOMPvUnBoHdcVJ9hqBOU3jZ0kmSa', '::1', NULL, NULL),
(7, 3, '$2y$10$Nbo9L7XZkBFUo9z3gqZCGuhFM4oxLCuMwbgQDWRKmsPwwW4f2WT3a', '::1', NULL, NULL),
(8, 3, '$2y$10$DqolqqP7S4a8DXbPZR/wo.GOKpKxJU7FC7M66cFKKhH.UNoVfHXTy', '::1', NULL, NULL),
(9, 3, '$2y$10$hrVd0m6/X3JJFbrjo3xOF.BErLzE1w0/Fh5xRrDvLdgx9w/KX4H2S', '::1', NULL, NULL),
(10, 3, '$2y$10$9/xFeo4ktwvAQfVRnBXHv.BFu93tybM3vP05h79IcXnrm/LKDeCVi', '::1', NULL, NULL),
(11, 3, '$2y$10$Bc.5nS/KjihyGqv/p.XPXecJhLqm5QyGgZDwYl7TcZNm2MKnK0YfW', '::1', NULL, NULL),
(12, 3, '$2y$10$oHRha1wsRiBiMKUDgne9.ebB8u.4OPbnK0T0NpLJW9o//JRiLZ.lq', '::1', NULL, NULL),
(13, 3, '$2y$10$48bJesp6FvcVnZzPK.YwWO.tSvu8U0QFDG/YrK6igqbgdOUtw9JCm', '::1', NULL, NULL),
(14, 3, '$2y$10$FCDG7604I9D5SXxBrTD.Z.Q4EgoUmzIGQJQilQkvdUsA.LB1.NLCK', '::1', NULL, NULL),
(15, 4, '$2y$10$WRFleQ8x85ouvlbv.1h37.f8W4xBvcmo7WdKCRzgR.IHwz6WbCHiW', '::1', NULL, NULL),
(16, 4, '$2y$10$syuXfZ/Ylrp046uWbe8xF.ROYFxN/w5LerQoVxFiJ.KGnweHyzMdO', '::1', NULL, NULL),
(17, 5, '$2y$10$gtRG4C5SUftD0NFwDE5Gk.jVCWjNyaOjoSFwCrWcWjL/sL.q0LWcW', '::1', NULL, NULL),
(18, 5, '$2y$10$8OvggiZ6waWEz34du1E8xe0jaKD36RYsmBT8.fp1VS6izOvS7IhnK', '::1', NULL, NULL),
(19, 5, '$2y$10$oZb8.3gysBjWuzmMB9CC3uBlYSt4LwFlpKIazOegBVNRvyMpEeor2', '::1', NULL, NULL),
(20, 5, '$2y$10$UAdhJ7S8usFa90JiSkwC7exsBB5HH4sc1sgsXWx.XXjgya.7WonRO', '::1', NULL, NULL),
(21, 5, '$2y$10$SNWNl5HYbvkd/nOo5XKr2uh91bDZt8fQhPJoqU5AD5YOsyRKVG0Gu', '::1', NULL, NULL),
(22, 5, '$2y$10$dt0chvmZPB/Y5WvCsREBE.XELSvD/rbY2xJwN0qQsTPff4l1RtSNu', '::1', NULL, NULL),
(23, 5, '$2y$10$RyWKpO2ZOw5z2HgjrAiqpeoBVY3y/YYPJ4F4ymuOAn3R5NnyvwgPK', '::1', NULL, NULL),
(24, 5, '$2y$10$8v7tWlmyEXvVwG68DJsFkOXyZ2P3W6S9QLEIr.xtXSaSSledxMtbq', '::1', NULL, NULL),
(25, 5, '$2y$10$4emy1iEkt9uqwZLBvSLaKebtPVdfV0pJnvuOvSOR3qj8MxPREfJxq', '::1', NULL, NULL),
(26, 5, '$2y$10$LUPnBrb9Q1MMKk4mzqKcvuzCG9cTElpxm5NAaeb8uzvdCbiZfW2YC', '::1', NULL, NULL),
(27, 5, '$2y$10$xgD76KM9oTqFO1eYJZP4dOTLbbVfacWtoUvdcZhNc0Y.ri13Dc/9y', '::1', NULL, NULL),
(28, 5, '$2y$10$Cr25A26y6IxcPEj0zwIA..8j31RZIz7Cz1/OpMRTCNNV4Ehv7UPyu', '::1', NULL, NULL),
(29, 5, '$2y$10$64wciS.3g6CRYv54lb2YNOjQxAu/fyycxBrMwxtxyzx3s.uaYAFFC', '::1', NULL, NULL),
(30, 5, '$2y$10$m7m1HGDs7uf1o4zAIwFH5OiB/Q/Sq0IDgltYsBuXMy7m3M4KIeeH.', '127.0.0.1', NULL, NULL),
(31, 5, '$2y$10$E9Kc/jcmpykQzLg45/Ov/eE8hoeGNjDF.fS3c0BOMywSdg2Ku/fuS', '127.0.0.1', NULL, NULL),
(32, 5, '$2y$10$hLBrv3XFF9vBB/FEv9X.TOZKjWf4n7lBLLxkL6GLgV8Q.x2kRnnKK', '::1', NULL, NULL),
(33, 5, '$2y$10$Q1Vkg6MTbuyAXUkCjFCaAOUqG/VOMWKzqbRUew7dJIXNQ65U00lRi', '::1', NULL, NULL),
(34, 5, '$2y$10$cWVAzmKt4UXjaf43e6aX0OxxdvNiY2VEiBNE1tbg2wN8sz/NlS7a6', '::1', NULL, NULL),
(35, 5, '$2y$10$.rvJb3eqfAdsBzURDFDtfOhI7T9oM02jaWFkUo9BcPmCBGj2UQjGK', '127.0.0.1', NULL, NULL),
(36, 5, '$2y$10$E7dmFv8ICU9ew/P6coZqduSsQ1BMgpqw0k30LdQrQN6qjRsL5kDpO', '::1', NULL, NULL),
(37, 5, '$2y$10$Sa4ESfhxnmE4uHvsjtRtj.3QaQflGy1dFV8qIS66btcxW17MavAh2', '::1', NULL, NULL),
(38, 5, '$2y$10$kDxobyudvpiBQe2Gr2Y98uZJLdU/uEnjYzFqq6320G4VzDGRKZgAa', '::1', NULL, NULL),
(39, 5, '$2y$10$FvJCV/YNI6TvGD2wcVzZteB0.ZiPYR4eiWgICJVq2DIloHbf0cJE.', '::1', NULL, NULL),
(40, 5, '$2y$10$n/jyoMWFY7WWv31zs6thTukwnjytIloqYNawRAZKITu/Gbck/PZS2', '::1', NULL, NULL),
(41, 5, '$2y$10$lLdohoNrQyOR5nCqnKOHjOiR7lNtuJTVF8PQCzITqIkBbJqa5kUsW', '::1', NULL, NULL),
(42, 5, '$2y$10$C/SZE/2f/mDMaplN9QFNUOr4dnhF3LyuSC4eWRoUFi8IXsEtsuloG', '::1', NULL, NULL),
(43, 5, '$2y$10$mN5rE0ZKM1mwHLZKnxU8E.YIpKo3iQAifvb0gIMS1l/04wtCqGyZC', '::1', NULL, NULL),
(44, 5, '$2y$10$CLwHN5MuAH2YmxueU98v1eDjBGnS2bkAkKKwPVW0jOSuJYvicEzma', '::1', NULL, NULL),
(45, 5, '$2y$10$F9ZExer081mDYoyRxp71yuLLNu3cND9Vd.jYtUR7J8rGdEiH/E2KC', '::1', NULL, NULL),
(46, 5, '$2y$10$JFRk34VbYQzNzilC56yp3eXgU5zezTnpd8MYnhhiFbjbx5tyBmv.C', '::1', NULL, NULL),
(47, 5, '$2y$10$VzkTeqhIX7OYkBa3fXbO6uZZyuPWAH/FysCZVApwJUvUjXIGEcZJu', '::1', NULL, NULL),
(48, 5, '$2y$10$Z4s2fZirZvxKT1AoEkO.n.wIp1SmYvREcsYFgu6IfvqPYUPhvVoY.', '::1', NULL, NULL),
(49, 5, '$2y$10$rHOl23zmmnFb6yjVXRd5fObKY1UaWK7oJHee.vnCpstQWtoSgwpcS', '::1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(10) UNSIGNED NOT NULL,
  `TOKEN` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIRST_NAME` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LAST_NAME` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GENDER` tinyint(1) DEFAULT NULL,
  `PHONE_NUMBER` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PHONE_CHECKED` tinyint(1) NOT NULL DEFAULT '0',
  `PROFILE_PICTURE` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COVER_PICTURE` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FACEBOOK` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LINKEDIN` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS_NUMBER` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS_STREET` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS_ZIP` int(11) DEFAULT NULL,
  `ADDRESS_CITY` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS_COUNTRY` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AVERAGE_REVIEW` double DEFAULT NULL,
  `RANK` int(11) DEFAULT NULL,
  `RANK_PROGRESS` int(11) DEFAULT NULL,
  `EMAIL` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EMAIL_CHECKED` tinyint(1) NOT NULL DEFAULT '0',
  `PASSWORD` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ANIMALS` tinyint(4) NOT NULL DEFAULT '0',
  `CIGARETTE` tinyint(4) NOT NULL DEFAULT '0',
  `CASH` double NOT NULL DEFAULT '0',
  `RULES_SIGNED` tinyint(1) NOT NULL DEFAULT '0',
  `LAST_CONNECTION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `TOKEN`, `FIRST_NAME`, `LAST_NAME`, `GENDER`, `PHONE_NUMBER`, `PHONE_CHECKED`, `PROFILE_PICTURE`, `COVER_PICTURE`, `FACEBOOK`, `LINKEDIN`, `ADDRESS_NUMBER`, `ADDRESS_STREET`, `ADDRESS_ZIP`, `ADDRESS_CITY`, `ADDRESS_COUNTRY`, `AVERAGE_REVIEW`, `RANK`, `RANK_PROGRESS`, `EMAIL`, `EMAIL_CHECKED`, `PASSWORD`, `ANIMALS`, `CIGARETTE`, `CASH`, `RULES_SIGNED`, `LAST_CONNECTION`, `created_at`, `updated_at`) VALUES
(1, '5a7341d8744d21.18949432', 'Kévin', 'Kévers', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kevinkevers@gmail.com', 0, '$2y$10$cx5bgKkZ4efgsg9pltNWWeXb9CJojEDQWlTeE0Jjodj/a/eVweT36', 0, 0, 0, 0, '2018-02-01 16:35:36', NULL, NULL),
(2, '5a7342063e1ed9.05452820', 'vqsdjfv', 'khsfb', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a@bc.be', 0, '$2y$10$7G.zqLUzhq5XYyxFB4PQn.Ji22LcQK9kEiPsAqz7G9MQIIF8WRtjW', 0, 0, 0, 0, '2018-02-01 16:36:22', NULL, NULL),
(3, '5a7795995a0429.32071441', 'Kévin', 'Kévers', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@gmail.com', 0, '$2y$10$GjO8BEUtiXeSfZXtnbFTdu.QqSnyrCEasx7FZ7mg50TTrE6WtVLmu', 0, 0, 0, 0, '2018-02-04 23:22:01', NULL, NULL),
(4, '5a8d465a7ac093.50479270', 'kqlsfbd', 'klhsdf', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@yolo.be', 0, '$2y$10$wcXBFF/vd0t6L/2Rhosjh.rY1UXrIKlkAqET9w4pSJrAOUDfo0gaK', 0, 0, 0, 0, '2018-02-21 10:13:46', NULL, NULL),
(5, '5a8ecf533b5355.82724297', 'Bonjour', 'Kévin', NULL, '499473028', 0, NULL, NULL, NULL, NULL, NULL, 'allo', NULL, NULL, NULL, NULL, NULL, NULL, 'hello@test.be', 0, '$2y$10$wAByqqQQdzkibGCWcRUgiej7erQdYY99K.YULKhlM5b4aSXas6SHe', 0, 0, 0, 0, '2018-03-03 13:19:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_badges`
--

CREATE TABLE `users_badges` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `BADGE` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_conversations`
--

CREATE TABLE `users_conversations` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `CONVERSATION` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `ID` int(10) UNSIGNED NOT NULL,
  `GROUP` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_languages`
--

CREATE TABLE `users_languages` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `LANGUAGE` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_works`
--

CREATE TABLE `users_works` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `WORK` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `ID` int(10) UNSIGNED NOT NULL,
  `USER` int(10) UNSIGNED NOT NULL,
  `SECTION` int(10) UNSIGNED NOT NULL,
  `DATE_MIN` date NOT NULL,
  `DATE_MAX` date NOT NULL,
  `ZIP` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE `works` (
  `ID` int(10) UNSIGNED NOT NULL,
  `WORKER` int(10) UNSIGNED NOT NULL,
  `ASKER` int(10) UNSIGNED NOT NULL,
  `PRICE` double NOT NULL,
  `STATUT` int(11) NOT NULL,
  `ADDRESS_NUMBER` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ADDRESS_STREET` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ADDRESS_ZIP` int(11) NOT NULL,
  `ADDRESS_CITY` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ADDRESS_COUNTRY` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SECTION` int(10) UNSIGNED NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `albums_user_foreign` (`USER`);

--
-- Indexes for table `badges`
--
ALTER TABLE `badges`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `contacts_main_foreign` (`MAIN`),
  ADD KEY `contacts_contact_foreign` (`CONTACT`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `conversations_work_foreign` (`WORK`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `groups_owner_foreign` (`OWNER`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `messages_user_foreign` (`USER`),
  ADD KEY `messages_conversation_foreign` (`CONVERSATION`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `notifications_user_foreign` (`USER`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `pictures_album_foreign` (`ALBUM`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `reviews_from_foreign` (`FROM`),
  ADD KEY `reviews_to_foreign` (`TO`),
  ADD KEY `reviews_work_foreign` (`WORK`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `sessions_user_foreign` (`USER`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users_badges`
--
ALTER TABLE `users_badges`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `users_badges_user_foreign` (`USER`),
  ADD KEY `users_badges_badge_foreign` (`BADGE`);

--
-- Indexes for table `users_conversations`
--
ALTER TABLE `users_conversations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `users_conversations_user_foreign` (`USER`),
  ADD KEY `users_conversations_conversation_foreign` (`CONVERSATION`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `users_groups_group_foreign` (`GROUP`),
  ADD KEY `users_groups_user_foreign` (`USER`);

--
-- Indexes for table `users_languages`
--
ALTER TABLE `users_languages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `users_languages_user_foreign` (`USER`),
  ADD KEY `users_languages_language_foreign` (`LANGUAGE`);

--
-- Indexes for table `users_works`
--
ALTER TABLE `users_works`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `users_works_user_foreign` (`USER`),
  ADD KEY `users_works_work_foreign` (`WORK`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `warnings_user_foreign` (`USER`);

--
-- Indexes for table `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `works_worker_foreign` (`WORKER`),
  ADD KEY `works_asker_foreign` (`ASKER`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `badges`
--
ALTER TABLE `badges`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_badges`
--
ALTER TABLE `users_badges`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_conversations`
--
ALTER TABLE `users_conversations`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_languages`
--
ALTER TABLE `users_languages`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_works`
--
ALTER TABLE `users_works`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `works`
--
ALTER TABLE `works`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `albums_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_contact_foreign` FOREIGN KEY (`CONTACT`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `contacts_main_foreign` FOREIGN KEY (`MAIN`) REFERENCES `users` (`ID`);

--
-- Constraints for table `conversations`
--
ALTER TABLE `conversations`
  ADD CONSTRAINT `conversations_work_foreign` FOREIGN KEY (`WORK`) REFERENCES `works` (`ID`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_owner_foreign` FOREIGN KEY (`OWNER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_conversation_foreign` FOREIGN KEY (`CONVERSATION`) REFERENCES `conversations` (`ID`),
  ADD CONSTRAINT `messages_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_album_foreign` FOREIGN KEY (`ALBUM`) REFERENCES `albums` (`ID`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_from_foreign` FOREIGN KEY (`FROM`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `reviews_to_foreign` FOREIGN KEY (`TO`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `reviews_work_foreign` FOREIGN KEY (`WORK`) REFERENCES `works` (`ID`);

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `users_badges`
--
ALTER TABLE `users_badges`
  ADD CONSTRAINT `users_badges_badge_foreign` FOREIGN KEY (`BADGE`) REFERENCES `badges` (`ID`),
  ADD CONSTRAINT `users_badges_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `users_conversations`
--
ALTER TABLE `users_conversations`
  ADD CONSTRAINT `users_conversations_conversation_foreign` FOREIGN KEY (`CONVERSATION`) REFERENCES `conversations` (`ID`),
  ADD CONSTRAINT `users_conversations_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_group_foreign` FOREIGN KEY (`GROUP`) REFERENCES `groups` (`ID`),
  ADD CONSTRAINT `users_groups_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `users_languages`
--
ALTER TABLE `users_languages`
  ADD CONSTRAINT `users_languages_language_foreign` FOREIGN KEY (`LANGUAGE`) REFERENCES `languages` (`ID`),
  ADD CONSTRAINT `users_languages_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `users_works`
--
ALTER TABLE `users_works`
  ADD CONSTRAINT `users_works_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `users_works_work_foreign` FOREIGN KEY (`WORK`) REFERENCES `works` (`ID`);

--
-- Constraints for table `warnings`
--
ALTER TABLE `warnings`
  ADD CONSTRAINT `warnings_user_foreign` FOREIGN KEY (`USER`) REFERENCES `users` (`ID`);

--
-- Constraints for table `works`
--
ALTER TABLE `works`
  ADD CONSTRAINT `works_asker_foreign` FOREIGN KEY (`ASKER`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `works_worker_foreign` FOREIGN KEY (`WORKER`) REFERENCES `users` (`ID`);
