var elixir = require('laravel-elixir');
elixir.config.sourcemaps = false;

const server = 'http://localhost:8000';
// Transform to CSS3
// PREFIX
// MINIFY if --production flag
// REFRESH SERVER
elixir(function(mix) {
    mix.sass(['pages/*.scss','main.scss'],"public/css/main.css")
});

elixir(function(mix) {
    mix.scripts('*.js', 'public/js/main.js');
});

elixir(function(mix) {
    mix.scripts('library/*.js', 'public/js/lib.js');
});

elixir(function(mix) {
    mix.scripts('external/*.js', 'public/js');
});

// ADMIN
elixir(function(mix) {
    mix.sass(['admin/*.scss','admin.scss'],"public/css/admin.css")
});

elixir(function(mix) {
    mix.scripts('admin/*.js', 'public/js/admin.js');
});