<?php
namespace App;
use Illuminate\Http\Request;

class Notification
{
    public function __construct($status,$message)
    {
        session_start();
        $data = [
            "status" => $status,
            "message" => $message
        ];

        session()->flash("notification",$data);
    }

}