<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

class CoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request){
        $page = Route::currentRouteName();
        $tmp = DB::table("content")->where("PAGE",$page)->get();
        $content = [];
        for($i = 0; $i < count($tmp); $i++){
            $content[$tmp[$i]->TAG] = $tmp[$i]->CONTENT;
        }

        if(view()->exists($page)){
            return view($page,
                [
                    "page" => $page,
                    "content" => $content
                ]
            );
        }
        else{
            return view("404");
        }
    }
}
