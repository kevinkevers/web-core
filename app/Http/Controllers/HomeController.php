<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("home");
    }
    public function indexGeneric(Request $request){
        $requested = $request->page;

        $page = [
            "accueil" => "home",
            "a-propos" => "about",
            "contact" => "contact",
            "coaching-mental" => "coaching-mental",
            "enfants-ados" => "enfants-ados",
            "image-de-soi" => "image-de-soi",
        ];

        if(!isset($page[$requested])){
            return view("404");
        }

        $tmp = DB::table("content")->where("PAGE",$page[$requested])->get();
        $content = [];
        for($i = 0; $i < count($tmp); $i++){
            $content[$tmp[$i]->TAG] = $tmp[$i]->CONTENT;
        }

        if(view()->exists($page)){
            return view("admin.$page",
                [
                    "page" => "editor",
                    "subpage" => $page,
                    "content" => $content
                ]
            );
        }
    }

    public function indexNews(Request $request){
        $page = "news";
        $tmp = DB::table("content")->where("PAGE",$page)->get();
        $content = [];
        for($i = 0; $i < count($tmp); $i++){
            $content[$tmp[$i]->TAG] = $tmp[$i]->CONTENT;
        }

        $articles = DB::table("blog")->get();


        if(view()->exists($page)){
            return view($page,
                [
                    "page" => $page,
                    "content" => $content,
                    "articles" => $articles
                ]
            );
        }
    }
}
