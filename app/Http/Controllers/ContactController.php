<?php

namespace App\Http\Controllers;

use App\Form;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact');
    }

    function sendMail(Request $request){
        $receiver = "kevinkevers@gmail.com";
        $mail = [
            "NAME" => $request->name,
            "EMAIL" => $request->mail,
            "PHONE" => $request->phone,
            "MESSAGE" => $request->message
        ];

        $headers =  'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        $subject = "Question de ".$mail["NAME"].".";
        $message = "
        <strong>Contact du client</strong>
        <br>Nom : ".$mail["NAME"]."
        <br>Email : ".$mail["EMAIL"]."
        <br>Téléphone : ".$mail["PHONE"]."
        <br>
        <br><strong>Message</strong>
        <br>
        ".$mail["MESSAGE"];
        $form = new Form();
        $form->checkInput("name",$mail["NAME"], "nom");
        $form->checkInput("phone",$mail["PHONE"], "téléphone");
        $form->checkMail("email",$mail["EMAIL"], "email");
        $form->checkInput("message",$mail["MESSAGE"],"message");
        if(count($form->getErrors()) > 0){
            return $form->getErrors();
        }
        else{
            mail($receiver,$subject,$message,$headers);
        }

    }
}
