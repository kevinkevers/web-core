<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mockery\Exception;

class LibraryController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){
        $images = DB::table("library")->orderBy("ID","desc")->get();
        return view("admin.core.library",[
            "tag" => $_GET["tag"],
            "images" => $images
        ]);
    }

    public function test(Request $request){
        echo $request->file('new-image')->getClientSize();
        return $request->file('new-image')->getClientSize();
    }

    public function create(Request $request){

        // UPLOAD THE IMAGE
        try{
            $filePath = $this->uploadImage($request,"temp");

            // ORIGINAL
            $this->resizeImage("original",$filePath);

            // COVER
            $this->resizeImage("1000",$filePath,1000);

            // COVER
            $this->resizeImage("300",$filePath,300);

            // IMAGE LIBRARY
            $this->resizeImage("150x150",$filePath,"150x150");

            // INSERT IN DB
            DB::table('library')->insert([
                    'PATH' =>  basename($filePath)
                ]
            );

            //$this->deleteDir("temp");
            return basename($filePath);
        }catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Delete a directory and all files in it
     *
     * @param $dirPath
     */
    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * Upload a file uploaded from $request
     *
     * @param Request $request
     * @param string $directory
     * @return mixed
     */
    public function uploadImage(Request $request, $directory = "temp"){
        // CHECK IF THE FILE IS NOT TOO BIG COMPARE TO SERVER CONFIG
        $maxFileSize = self::max_file_size();

        if($maxFileSize > $request->file('new-image')->getClientSize()) {
            $path = "public/".$directory;
            $rawUrl = $request->file("new-image")->store($path);

            // replace public by /s for accessing from URL
            return str_replace("public","", $rawUrl);
        }else{
            // FILE TOO BIG
            throw new Exception("Your file is too big, max size is ". ini_get("upload_max_filesize"));
        }
    }

    public function max_file_size(){
        return self::parse_size(ini_get("upload_max_filesize"));
    }

    public function file_upload_max_size() {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $post_max_size = self::parse_size(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = self::parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    // transform Mega Giga ... in numbers
    public function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }
    public function testGet(Request $request){
        $filePath = "temp/0eKOrimwRm2LhwBJU5fhhcylrELV83Q5R3M7b042.jpeg";

        $this->resizeImage("3000",$filePath,"3000x3000");

        return "done";
    }

    /**
     * This function resize and crop if needed an image from storage
     *
     * @param $targetPath - Path where the file will be created (create dir if doesn't exist)
     * @param $filePath - Path of the source file
     * @param $size - wished size (100x100 - 200xauto - autox100)
     * @return string
     */

    static function resizeImage($targetPath, $filePath, $size = "auto") {
        // NO CROP ON START
        $crop = false;

        // MIN SIZE FOR THE SOURCE IMAGE
        $minWidth = 100;
        $minHeight = 100;

        // GET FILE AND CREATE IMAGE RESOURCE FROM STRING
        $file = imagecreatefromstring(Storage::disk('public')->get($filePath));

        // CHECK IF FILE EXISTS
        $exists = Storage::disk('public')->exists($filePath);

        if(!$exists){
            throw new Exception("The file was not found at ".$filePath);
        }

        // GET MIME
        $mime = Storage::disk('public')->mimeType($filePath);

        // RETURN CORRESPONDING ELEMENTS AND FUNCTIONS DEPENDING ON THE IMAGE TYPE
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Unknown image type.');
        }

        // Get the width and the height of the currrent image
        $width = imagesx($file);
        $height = imagesy($file);

        // CHECK IF IT FITS MINIMUM SIZE
        if($width < $minWidth || $height < $minHeight){
            throw new Exception("Image uploaded is too small : need at least ".$width."px x ".$height."px (can be changed within function)");
        }

        // =================================
        // ======== SIZE AND CROP ==========
        // =================================

        // CHECK IF A SIZE HAS BEEN INPUT
        if($size == null){
            throw new Exception("No size defined");
        }
        else{
            // ONLY 1 VALUE ENTERED
            if(!stripos($size, 'x') && $size !== "auto"){
                if($width >= $height){
                    $newWidth = intval($size);
                    // compute the new size
                    $newHeight = ($height / $width) * $newWidth;
                }
                else{
                    $newHeight = intval($size);
                    // compute the new size
                    $newWidth = ($width / $height) * $newHeight;
                }
            }
            // NO SIZE CHANGE
            elseif($size === "auto"){
                $newHeight  = $height;
                $newWidth  = $width;
            }
            // SEVERAL VALUES ENTERED (100x200)
            else{
                // GET THE SIZE
                list($targetWidth, $targetHeight) = explode("x",$size);
                // NUMERIC HEIGHT - AUTO WIDTH (100xauto)
                if($targetWidth === "auto" && is_numeric($targetHeight)){
                    $newHeight = intval($targetHeight);
                    // compute the new size
                    $newWidth = ($width / $height) * $newHeight;
                }
                // NUMERIC WIDTH - AUTO HEIGHT (autox100)
                elseif($targetHeight === "auto" && is_numeric($targetWidth)){
                    // Transform string to digit
                    $newWidth = intval($targetWidth);
                    // compute the new size
                    $newHeight = ($height / $width) * $newWidth;

                }
                // NUMERIC WIDTH - NUMERIC HEIGHT (100x100)
                elseif(is_numeric($targetHeight) && is_numeric($targetWidth)){
                    // ALLOW THE IMAGE TO BE CROPPED
                    $crop = true;

                    // CHECK THE COEFFICIENT TO FIND THE SMALLEST ONE
                    $coeX = $width/$targetWidth;
                    $coeY = $height/$targetHeight;
                    if($coeX <= $coeY){
                        $newWidth = intval($targetWidth);
                        $newHeight = $height/$coeX;
                    }
                    else{
                        $newHeight = intval($targetHeight);
                        $newWidth = $width/$coeY;
                    }
                }
                // PROBLEM WITH VALUE ENTERED
                else{
                    throw new Exception("There is a problem with the values entered.". $size. " is not valid");
                }
            }
        }
        // RESIZE IMAGE
        $tmpFile = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmpFile, $file, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        // CROP IF NEEDED AND FLAG OPEN
        if($crop){
            $tmpFile = imagecrop($tmpFile, ['x' => 0, 'y' => 0, 'width' => intval($targetWidth), 'height' => intval($targetHeight)]);
        }
        // OPEN BUFFER
        ob_start();
        imagejpeg($tmpFile);

        // GET THE STREAM OUTPUT
        $imageData = ob_get_clean();

        // GET FILENAME
        $filename = $targetPath."/".basename($filePath);

        // STORE IT
        return Storage::disk('public')->put($filename,$imageData);

    }
}