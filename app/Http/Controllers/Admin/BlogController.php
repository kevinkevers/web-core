<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mockery\Exception;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request){
        $page = $request->page ?? "";
        $subpage = $request->subpage ?? "";

        $content = DB::table("blog")->get();

        return view("admin.blog.index",[
                "page" => $page,
                "subpage" => $subpage,
                "content" => $content
            ]);
    }

    // DISPLAY ARTICLE PAGE
    public function view(Request $request){
        $page = $request->page ?? "";
        $subpage = $request->subpage ?? "";
        $articleID = $request->param ?? "";

        // GET ALL THE CONTENT
        $content = DB::table("blog")->where("ID",$articleID)->first();
        $content = json_decode(json_encode($content), true);
        return view("admin.blog.article",[
            "page" => $page,
            "subpage" => $subpage,
            "content" => $content
        ]);
    }

    public function saveArticle(Request $request){
        $inputs = $request->input("input");
        $subpage = $request->input("subpage");

        $articleID = null;

        if(isset($inputs[0]) && $inputs[0]["name"] === "new_or_update"){
            if($inputs[0]["value"] !== "" && $inputs[0]["value"] !== "new"){
                $articleID = $inputs[0]["value"];
            }

           $image = $inputs[1]["value"];
           $title = $inputs[2]["value"];
           $content = $inputs[3]["value"];
        }else{
            abort(500, "new_or_update input not specified");
        }

        if($articleID === null){
            // CREATE
            DB::table('blog')
                ->insert([
                    "COVER_IMAGE" => $image,
                    "TITLE" => $title,
                    "CONTENT" => $content,
                    "AUTHOR" => "Virginie Lezaack",
                    "PUBLISH_DATE" => date("Y-m-d H:i:s")
                ]);

            $message = "News créée !";
            new Notification("success",$message);
        }
        else{
            // UPDATE
            DB::table('blog')
                ->where('ID', $articleID)
                ->update([
                    "COVER_IMAGE" => $image,
                    "TITLE" => $title,
                    "CONTENT" => $content,
                ]);

            $message = "News mise à jour !";
            new Notification("success",$message);
        }

    }
}
