<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App;
use Illuminate\Support\Facades\DB;
use App\Notification;
use App\Exceptions;
use Illuminate\Routing\Route;
use Carbon\Carbon;


class ManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        return view("admin.manager");
    }

    public function indexSettings(Request $request)
    {
        $page = $request->page;
    }

    public function indexEditor(Request $request){
        $page = $request->page;

        // GET ALL THE CONTENT
        $tmp = DB::table("content")->where("PAGE",$page)->get();
        $content = [];

        for($i = 0; $i < count($tmp); $i++){
            $content[$tmp[$i]->TAG] = $tmp[$i]->CONTENT;
        }

        if(isset($request->tab)){
            $tab = $request->tab;
            if($tab === "page-infos"){
                return view("admin.editor.generic.page-info",
                    [
                        "page" => "editor",
                        "subpage" => $page,
                        "tab" => $tab,
                        "content" => $content
                    ]
                );
            }
        }
        else{
            if(view()->exists("admin.editor.".$page)){
                return view("admin.editor.$page",
                    [
                        "page" => "editor",
                        "subpage" => $page,
                        "content" => $content
                    ]
                );
            }
            else{
                echo "There is a problem with routing, please contact your webmaster. Route = ".$page;
            }
        }
    }

    public function ajax($url){
        if(isset($_GET["tag"])){
            return view("admin.".$url,["tag" => $_GET["tag"]]);
        }
        else{

        }
    }

    public function saveData(Request $request){
        $inputs = $request->input("input");
        $page = $request->input("page");

        for($i = 0; $i < count($inputs);$i++)
        {
            $tag = $inputs[$i]["name"];
            $content = $inputs[$i]["value"];
            $tmp = DB::table("content")->where(["TAG" => $tag, "page" => $page])->first();
            // Exist => update
            if($tmp != null){
                DB::table('content')
                    ->where('ID', $tmp->ID)
                    ->update(['CONTENT' => $content]);
            }
            else{
                DB::table('content')
                    ->insert([
                            "TAG" => $tag,
                            "CONTENT" => $content,
                            "PAGE" => $page
                        ]);
            }
        }
        $message = "Modifications bien enregistrées";
        new Notification("success",$message);
        return 1;
    }

    // RETURN FILE WITH API
    public function moduleLauncher(Request $request){

        // Retrieving variables
        $page = $request->page ?? "";
        $subpage = $request->subpage ?? "";
        $action = $request->action ?? "index";
        $module = $request->module;
        $param = $request->param;
        $controller = ucfirst($module)."Controller";
        $modules = [
            [
                "name" => "blog",
                "allowedFunction" => ["add"]
            ]
        ];

        if($this->moduleIsInArray($module,$modules)){
            try {
                if (method_exists("App\Http\Controllers\Admin\\".$controller, $action)){
                    echo App::call("App\Http\Controllers\Admin\\".$controller."@".$action,[
                        "page" => $page,
                        "subpage" => $subpage,
                        "param" => $param
                    ]);
                }
                else{
                    abort(403, 'Controller or Method not found');
                }
            } catch (Exception $e) {
                abort(403, 'Controller not found');
            }
            }
            else{
                abort(403, 'Module not found or not allowed');
            }
        }

        public function moduleIsInArray($module,$modules){
            for($i = 0; $i < count($modules); $i++){
                if($module === $modules[$i]["name"]){
                    return true;
                }
            }
            return false;
        }

    // RETURN FILE WITH API
    public function getFile($filePath){
        $exists = Storage::disk('public')->exists($filePath);
        if($exists){
            $mime = $this->extToMime($filePath);
            return response(Storage::disk('public')->get($filePath))->header('Content-type',$mime);
        }
        else{
            return 0;
        }
    }

    public function extToMime($filePath){
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        $tmp = explode('.', $filePath);
        $ext = end($tmp);
        return $mime_types[$ext];
    }
}
