<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mockery\Exception;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $user = "admin";
        $password = "vlezaack";

        if($request->password === $password && $request->username === $user){
            session(['username' => $request->username]);
            session(['connected' => true]);
            session()->forget('error');
            return redirect('admin/dashboard');
        }
        else{
            session()->put('error'," L'utilisateur ou le mot de passe est incorrect.");
            return redirect('admin/login');
        }
    }

    function logout(Request $request){
        session()->forget('connected');
        session()->forget('username');
        return redirect('admin/login');
    }

    public function index(Request $request){
        return view("admin.login");
    }
}
