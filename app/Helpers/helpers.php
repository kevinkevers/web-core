<?php
function date_parser($date,$type = "full"){
    $month4 = [
        "janv",
        "fevr",
        "mars",
        "avri",
        "mai",
        "juin",
        "juil",
        "août",
        "sept",
        "octo",
        "nove",
        "dece"
    ];
    $monthFull = [
        "janvier",
        "février",
        "mars",
        "avril",
        "mai",
        "juin",
        "juillet",
        "août",
        "septembre",
        "octobre",
        "novembre",
        "décembre"
    ];

    switch($type){
        case "full":
            $dateFormat = $monthFull;
            break;

        case "4":
            $dateFormat = $month4;
            break;
        default:
            $dateFormat = $monthFull;
            break;
    }

    $splitDate = explode("-",$date);
    $parsedDate = $splitDate[2]." ".$dateFormat[intval($splitDate[1]) - 1]." ".$splitDate[0];
    return $parsedDate;
}

function get_city($address){
    $splitAddress = preg_split( "/[\s,]+/", $address );
    $city = $splitAddress[count($splitAddress) -1];
    return $city;
}

function clean_hour($hour){
    return substr($hour,0,-3);
}

function svg($path){
    if(file_exists($path)) {
        return file_get_contents($path);
    }
    else{
        return "File not found at : ".$path;
    }
}

?>